package TP1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File monFile = new File("fichier.txt"); // cr�ation logique l'objet
		try {
			FileWriter monFileWrit= new FileWriter(monFile);
			monFileWrit.write("hello Word \n"); //string + retour � la ligne
			monFileWrit.write(86); //nombre
			monFileWrit.write('a'); //char
			monFileWrit.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// on peut tout faire en 1 ligne : 
		// FileWriter monFileWrit = new FileWriter("fichier.txt");
		// ("fichier.txt",1) pour �crire � la fin du fichier et ne pas �craser.
		
		
	}

}

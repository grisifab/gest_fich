package TP3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws IOException{
	
	try {
		File file = new File("fichier.txt");
		FileReader fr = new FileReader(file);
		int cha = fr.read();
		while (cha != -1) {
			System.out.print((char) cha);
			cha = fr.read();
		}
		fr.close();
	}
	catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	
	
	}
}

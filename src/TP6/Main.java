package TP6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		
		System.out.println("------------------La chanson---------------------");
		Scanner sc = new Scanner(System.in);
		String maLigne = new String();
		File monFile = new File("chanson.txt"); // cr�ation logique l'objet
		
		try {
			FileWriter monFileWrit= new FileWriter(monFile);
			BufferedWriter bw = new BufferedWriter(monFileWrit);
			
			for (int i = 1; i <=4; i++) {
				System.out.println("Veuillez rentrez la ligne N�" + i);
				maLigne = sc.nextLine();	
				bw.write(maLigne);
				bw.newLine();
			}
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sc.close();
		
		System.out.println("------------------informations sur le fichier---------------------");
		
		File myFile = new File("/Users/PC20/eclipse-workspace/Gest_Fich/chanson.txt");// chemin obligatoire ! pour parent
		
		if(myFile.exists()) {
			System.out.println(myFile.getName() + " existe.");
			System.out.println("Il fait " + myFile.length() + " octets");
			System.out.println("Et se trouve l� : " + myFile.getAbsolutePath());
		}
		if(myFile.isFile()){
			System.out.println(myFile.getName() + " est un fichier");
		}
		
		System.out.println("------------------informations sur le dossier---------------------");
		File myRep = new File(myFile.getParent()); // fonctionne uniquement si le fichier est choisi avec son chemin ! 
		File[] files = myRep.listFiles();
		
		if(myRep.isDirectory()){
			System.out.println(myRep.getAbsolutePath() + " est un r�pertoire");
		}
		
		System.out.println("Il contient : ");
		
		for (File file: files) {
            System.out.println(file.getAbsolutePath());
        }
		
		System.out.println("------------------La chanson---------------------");
		
		try {
			FileReader fr = new FileReader(myFile);
			BufferedReader br = new BufferedReader(fr);
			maLigne = br.readLine();
			while (maLigne != null) {
				System.out.println(maLigne);
				maLigne = br.readLine();
			}
			fr.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}

package TP5;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File myFile = new File("/Users/PC20/Desktop/imprim/fifi.txt");
		File myRep = new File("/Users/PC20/Desktop/imprim");
		File[] files = myRep.listFiles();
		
		if(myFile.exists()) {
			System.out.println(myFile.getName() + " existe.");
			System.out.println("Il fait " + myFile.length() + " octets");
			System.out.println("Et se trouve l� : " + myFile.getAbsolutePath());
		}
		if(myFile.isFile()){
			System.out.println(myFile.getName() + " est un fichier");
		}
		
		System.out.println("\n---------------------------\n");
		
		if(myRep.exists()) {
			System.out.println(myRep.getName() + " existe");
			System.out.println("Il fait " + myRep.length() + " octets");
			System.out.println("Et se trouve l� : " + myRep.getAbsolutePath());
		}
		
		if(myRep.isDirectory()){
			System.out.println(myRep.getAbsolutePath() + " est un r�pertoire");
		}
		
		System.out.println("Il contient : ");
		
		for (File file: files) {
            System.out.println(file.getAbsolutePath());
        }
	}

}

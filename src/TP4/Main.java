package TP4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws IOException{
	
	try {
		File file = new File("fichier.txt");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String chaine = br.readLine();
		while (chaine != null) {
			System.out.println(chaine);
			chaine = br.readLine();
		}
		fr.close();
	}
	catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	
	
	}
}
